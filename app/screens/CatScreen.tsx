import React from 'react';
import { Button, Image, StyleSheet, Text, View } from 'react-native';
import { useEffect, useState } from 'react';
import { RouteParams } from '../navigation/RootNavigator';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/core';


const CatScreen = () => {
    const [img, setImg] = useState(' ')

    const getCat = () => {
        fetch('http://aws.random.cat/meow')
            .then((res) => {
                return res.json()
            }).then((data) => {
                setImg(data.file)
            })
    }

    useEffect(() => {
        getCat()
    }, [])
    return (
        <View style={styles.container}>
            <View style={styles.text}>
                <Text>T'aime les chats ? </Text>
            </View>
            <Image
                source={{ uri: img }}
                style={styles.img}
            />
            <Button
                onPress={getCat}
                title="Encore un chat"
                color="#841584"
                accessibilityLabel="Learn more about this purple button"
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    img: {
        width: '80%',
        height: '65%',
        marginBottom: 20
    },
    text: {
        marginBottom: 20
    }
});

export default CatScreen;