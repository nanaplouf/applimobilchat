import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import CatScreen from '../screens/CatScreen';
import DogScreen from '../screens/DogScreen';
import HomeScreen from '../screens/HomeScreen';
import PikachuScreen from '../screens/PikachuScreen';
import RedPandaScreen from '../screens/RedPandaScreen';

export type RouteParams = {
    Cat: undefined,
    Dog: undefined,
    Home: undefined,
    Pikachu: undefined,
    RedPanda: undefined,
}

const Stack = createNativeStackNavigator<RouteParams>();

export const RootNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Group>
                <Stack.Screen
                    name="Home"
                    component={HomeScreen}
                />
                <Stack.Screen
                    name="Cat"
                    component={CatScreen}
                />
                <Stack.Screen
                    name="Dog"
                    component={DogScreen}
                />
                <Stack.Screen
                    name="Pikachu"
                    component={PikachuScreen}
                />
                <Stack.Screen
                    name="RedPanda"
                    component={RedPandaScreen}
                />
            </Stack.Group>
        </Stack.Navigator>
    );
};