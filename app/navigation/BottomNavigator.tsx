import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import CatScreen from '../screens/CatScreen';
import DogScreen from '../screens/DogScreen';

const Tab = createMaterialBottomTabNavigator();

export const RootNavigator = () => {
    return (
        <Tab.Navigator>
            <Tab.Group>
                <Tab.Screen name="Cat"
                    component={CatScreen}
                    options={{
                        tabBarLabel: 'Cat',
                        tabBarIcon: ({ color }) => (
                            <MaterialCommunityIcons name="home" color={color} size={26} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Dog"
                    component={DogScreen}
                    options={{
                        tabBarLabel: 'Dog',
                        tabBarIcon: ({ color }) => (
                            <MaterialCommunityIcons name="home" color={color} size={26} />
                        ),
                    }}
                />
            </Tab.Group>
        </Tab.Navigator>
    );
};